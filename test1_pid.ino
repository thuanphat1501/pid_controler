// #include <util/atomic.h> // For the ATOMIC_BLOCK macro
#include"analogWrite.h"

#define ENCA1 4 // YELLOW
#define ENCB1 16 // WHITE
#define PWM 21
#define IN1 22
#define IN2 23

#define ENCA2 17 // YELLOW
#define ENCB2 5 // WHITE
#define PWM 12
#define IN3 14
#define IN4 27

#define M0 26 // mode 0
#define M1 25 // mode 1
#define M2 33
#define STEP 32
#define DIR 35

#define TX 1
#define RX 0

volatile int posi = 0; // specify posi as volatile: https://www.arduino.cc/reference/en/language/variables/variable-scope-qualifiers/volatile/
long prevT = 0;
float eprev = 0;
float eintegral = 0;
int pos = 0; 


void setup() {
  Serial.begin(9600);
  pinMode(ENCA1,INPUT);
  pinMode(ENCB1,INPUT);
  attachInterrupt(digitalPinToInterrupt(ENCA1),readEncoder1,RISING);
  
  pinMode(PWM,OUTPUT);
  pinMode(IN1,OUTPUT);
  pinMode(IN2,OUTPUT);
 

  pinMode(ENCA2,INPUT);
  pinMode(ENCB2,INPUT);
  attachInterrupt(digitalPinToInterrupt(ENCA2),readEncoder2,RISING);
  
  pinMode(PWM,OUTPUT);
  pinMode(IN3,OUTPUT);
  pinMode(IN4,OUTPUT);
  Serial.println("target pos");
}

void loop() {

  // set target position
  int target = 1200;
  // int target = 250*sin(prevT/1e6);

  // PID constants
  float kp = 1;
  float kd = 0.025;
  float ki = 0.0;

  // time difference
  long currT = micros();
  float deltaT = ((float) (currT - prevT))/( 1.0e6 );
  prevT = currT;

  // Read the position in an atomic block to avoid a potential
  // misread if the interrupt coincides with this code running
  // see: https://www.arduino.cc/reference/en/language/variables/variable-scope-qualifiers/volatile/
 //khoi phuc lai 
  //int pos = 0; 
  // ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
  //   pos = posi;
  // }
  
  // error
  int e = pos - target;

  // derivative
  float dedt = (e-eprev)/(deltaT);

  // integral
  eintegral = eintegral + e*deltaT;

  // control signal
  float u = kp*e + kd*dedt + ki*eintegral;

  // motor power
  float pwr = fabs(u);
  if( pwr > 255 ){
    pwr = 255;
  }

  // motor direction
  int dir = 1;
  if(u<0){
    dir = -1;
  }

  // signal the motor
  setMotor1(dir,pwr,PWM,IN1,IN2);

  setMotor2(dir,pwr,PWM,IN3,IN4);

  // store previous error
  eprev = e;

  Serial.print(target);
  Serial.print(" ");
  Serial.print(pos);
  Serial.println();
}

void setMotor1(int dir, int pwmVal, int pwm, int in1, int in2){
  analogWrite(pwm,pwmVal);
  if(dir == 1){
    digitalWrite(in1,HIGH);
    digitalWrite(in2,LOW);
  }
  else if(dir == -1){
    digitalWrite(in1,LOW);
    digitalWrite(in2,HIGH);
  }
  else{
    digitalWrite(in1,LOW);
    digitalWrite(in2,LOW);
  }  
}

void setMotor2(int dir, int pwmVal, int pwm, int in3, int in4){
  analogWrite(pwm,pwmVal);
  if(dir == 1){
    digitalWrite(in3,HIGH);
    digitalWrite(in4,LOW);
  }
  else if(dir == -1){
    digitalWrite(in3,LOW);
    digitalWrite(in4,HIGH);
  }
  else{
    digitalWrite(in3,LOW);
    digitalWrite(in4,LOW);
  }  
}

void readEncoder1(){
  int b1 = digitalRead(ENCB1);
  if(b1 > 0){
    pos++;
  }
  else{
    pos--;
  }
}
void readEncoder2(){
  int b2 = digitalRead(ENCB2);
  if(b2 > 0){
    pos++;
  }
  else{
    pos--;
  }
}